function toShowNavigation() {
    const icon = document.getElementById('icon');
    const navigation = document.getElementsByClassName('navigation');

    if (navigation[0].style.display === "none") {
        navigation[0].style.display = "flex";
    }
    else {
        navigation[0].style.display = "none";
    }
}
